const exec = require('child_process').spawn

const { relay_path, web_path, relays, clients, start_relay, start_client } = process.env

const START_RELAY_PORT = parseInt(start_relay || '7447', 10)
const START_WEB_PORT = parseInt(start_client || '3000', 10)
let nrelays = parseInt(relays || '2', 10)
let nclients = parseInt(clients || '3', 10)

for (var i=0; i<nrelays; i++) {
  const port = START_RELAY_PORT + i
  const db = `relay${port}.db`
  const p = exec(relay_path, {
    env: {
      SQLITE_DATABASE: db,
      PORT: port
    }
  })
  p.stdout.on('data', (data) => {
    console.log(`stdout: ${data}`);
  })
  p.stderr.on('data', (data) => {
    console.log(`listener ${port}: ${data}`);
  })
  console.log(`process started for port: ${port}, db: ${db}`)
}

const express = require('express')


for (var i = 0; i < nclients; i++) {
  const app = express()
  app.use(express.static(web_path))
  const port = START_WEB_PORT+i
  app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
  })
}

