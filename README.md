### install
```
git clone https://codeberg.org/rsbondi/nostr-launch.git
cd nostr-launch
npm install
```

### set up environment

```
export relay_path=path/to/relay-executable
export web_path=path/to/web-static-build
npm start
```

or

```
relay_path=path/to/relay-executable web_path=path/to/web-static-build npm start
```

### additional environment settings

```bash
### relays

# number of relays to launch
relays=n # default 2

# start port number for relays, will increment by 1 for each `relays`
start_relay=n # default 7447

### clients

# number of clients to launch
clients=n # default 3

# start port number for clients, will increment by 1 for each `client`
start_client=n # default 3000

```

### usage

open a browser tab for each port ex. `localhost:3000` and in the client start connecting to relays ex. `ws://localhost:7447`

